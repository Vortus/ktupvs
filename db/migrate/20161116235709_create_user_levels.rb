class CreateUserLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :user_levels do |t|
      t.string :title
      t.integer :level

      t.timestamps
    end
  end
end
