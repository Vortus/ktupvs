module ApplicationHelper

  def link_to_add_test_inputs(name)
    link_to name, '#', class: "add_test_inputs_fields", :"data-field" => "<p>
    <div class=\"pull-left\"><input class='text optional' type='text'  name='exercise[test_inputs][]' id='exercise_'></div>
    <div class=\"pull-right\">TEST</div></p><hr>"
  end
end
