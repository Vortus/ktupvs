class UserLevelsController < ApplicationController
  before_action :authenticate_user!

  def index
    @user_levels = UserLevel.all.order('updated_at DESC')
  end

  def show
    # TODO
  end

  def new
    @user_level = UserLevel.new
  end

  def edit
    @user_level = UserLevel.find(params[:id])
  end

  def update
    @user_level = UserLevel.find(params[:id])
    if @user_level.update(user_level_params)
      flash[:success] = "User level was successfully edited!"
      redirect_to user_level_path(@user_level)
    else
      flash[:alert] = @user_level.errors.full_messages
      render 'edit'
    end
  end

  def destroy
    @user_level = UserLevel.find(params[:id])
    if @user_level.destroy
      flash[:success] = "User Level was removed!"
    else
      flash[:error] = @user_level.errors.full_messages
    end
    redirect_to user_levels_path
  end

  def create
    @user_level = UserLevel.new(user_level_params)
    if @user_level.save
      flash[:success] = "User Level has been created!"
      redirect_to user_levels_path
    else
      flash[:error] = @user_level.errors.full_messages
      redirect_to new_user_level_path
    end
  end

  private
    def user_level_params
      params.require(:user_level).permit(:title, :level)
    end
end
