class RemoveTestOutputsFromExercises < ActiveRecord::Migration[5.0]
  def change
    remove_column :exercises, :test_outputs, :text
  end
end
