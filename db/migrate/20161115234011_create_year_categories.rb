class CreateYearCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :year_categories do |t|
      t.string :title
      t.boolean :open

      t.timestamps
    end
  end
end
