json.(@solution, :status, :error, :file_name_input, :file_name_output, :language, :code, :time_limit, :size_limit)

json.test_data(@exercise.exercise_tests) do |exercise_test|
  json.test_input exercise_test.test_input
  json.test_output exercise_test.test_output
end
