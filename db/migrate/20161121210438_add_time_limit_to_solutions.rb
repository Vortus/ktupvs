class AddTimeLimitToSolutions < ActiveRecord::Migration[5.0]
  def change
    add_column :solutions, :time_limit, :integer
  end
end
