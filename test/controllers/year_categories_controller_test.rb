require 'test_helper'

class YearCategoriesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get year_categories_index_url
    assert_response :success
  end

  test "should get show" do
    get year_categories_show_url
    assert_response :success
  end

  test "should get new" do
    get year_categories_new_url
    assert_response :success
  end

  test "should get edit" do
    get year_categories_edit_url
    assert_response :success
  end

end
