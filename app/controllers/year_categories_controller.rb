class YearCategoriesController < ApplicationController
  before_action :authenticate_user!

  def index
    @year_categories = YearCategory.all.order('updated_at DESC')
  end

  def show
    # TODO
  end

  def new
    @year_category = YearCategory.new
  end

  def edit
    @year_category = YearCategory.find(params[:id])
  end

  def update
    @year_category = YearCategory.find(params[:id])
    if @year_category.update(year_category_params)
      flash[:success] = "Year Category was successfully edited!"
      redirect_to year_category_path(@year_category)
    else
      flash[:alert] = @year_category.errors.full_messages
      render 'edit'
    end
  end

  def destroy
    @year_category = YearCategory.find(params[:id])
    if @year_category.destroy
      flash[:success] = "Year Category was removed!"
    else
      flash[:error] = @year_category.errors.full_messages
    end
    redirect_to year_categories_path
  end

  def create
    @year_category = YearCategory.new(year_category_params)
    if @year_category.save
      flash[:success] = "Year Category has been created!"
      redirect_to year_categories_path
    else
      flash[:error] = @year_category.errors.full_messages
      redirect_to new_year_category_path
    end
  end

  private
    def year_category_params
      params.require(:year_category).permit(:title)
    end
end
