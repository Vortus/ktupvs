class AddSizeLimitToSolutions < ActiveRecord::Migration[5.0]
  def change
    add_column :solutions, :size_limit, :integer
  end
end
