# Requests an API to solve ID solution.
class ConnectionWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(id)
    path = "http://example.com/services/index.php?status=add"
    key = "dskljasd;ijfmoesfcwef65sad4df3as54d3431FDNVSDLKMVLS;DC12DSC234c2sd54v53sdv1sd351513DS5CSDXalk" # Key
    response = HTTParty.get("#{path}&id=#{id}&key=#{key}") # Get the response.
    ReviewSolutionWorker.perform_async(id) unless response.nil? # Review if solution is completed.
  end
end
