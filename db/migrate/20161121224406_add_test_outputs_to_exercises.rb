class AddTestOutputsToExercises < ActiveRecord::Migration[5.0]
  def change
    add_column :exercises, :test_outputs, :text, array: true, default: []
  end
end
