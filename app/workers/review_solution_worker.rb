# Checks if solution is solved.
class ReviewSolutionWorker
  include Sidekiq::Worker

  # Retry worker after 10 seconds
  sidekiq_retry_in do |count| 10 end

  def perform(id)
    path = "http://example.com/services/index.php?status=information"
    key = "dskljasd;ijfmoesfcwef65sad4df3as54d3431FDNVSDLKMVLS;DC12DSC234c2sd54v53sdv1sd351513DS5CSDXalk" # Key
    response = HTTParty.get("#{path}&id=#{id}&key=#{key}") # Get the response.

    # Cancel worker if no response received.
    if response.nil?
      @solution.update_attribute(status: "failed")
      return false
    end

    # Retry review worker if not completed.
    if response["status"] == "processing"
      raise "#{id} solution still processing..."
    else # Update data.
      @solution.update_attribute(status: response["status"])
      @solution.update_exercise_status # Update exercise status.
    end
  end
end
