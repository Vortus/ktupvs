require 'test_helper'

class UserLevelsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get user_levels_index_url
    assert_response :success
  end

  test "should get show" do
    get user_levels_show_url
    assert_response :success
  end

  test "should get new" do
    get user_levels_new_url
    assert_response :success
  end

  test "should get edit" do
    get user_levels_edit_url
    assert_response :success
  end

end
