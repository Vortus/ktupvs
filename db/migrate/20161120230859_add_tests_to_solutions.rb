class AddTestsToSolutions < ActiveRecord::Migration[5.0]
  def change
    add_column :solutions, :tests, :json
  end
end
