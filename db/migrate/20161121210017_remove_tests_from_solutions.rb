class RemoveTestsFromSolutions < ActiveRecord::Migration[5.0]
  def change
    remove_column :solutions, :tests, :json
  end
end
