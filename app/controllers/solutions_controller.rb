class SolutionsController < ApplicationController
  before_action :authenticate_user!, except: [:json]

  # Show solution
  def show
    @solution = Solution.find(params[:id])
    @exercise = @solution.exercise
  end

  # Filter exercise solutions
  def filter
    @exercise = Exercise.find(params[:id])
    @solutions = Solution.where({ user_id: current_user.id, exercise_id: @exercise.id })
  end

  # New solution.
  def new
    @solution = Solution.new
    @exercise = Exercise.find(params[:id])
    session[:exercise_id] = params[:id]
  end

  # Create solution.
  def create
    uploaded_file = params[:solution_file]
    if uploaded_file
      exercise = Exercise.find(session[:exercise_id])

      @solution = Solution.new
      @solution.code = uploaded_file.read
      @solution.file_name_input = uploaded_file.original_filename
      @solution.file_name_output = @solution.file_name_input + '.out'
      @solution.exercise = exercise
      @solution.user = current_user
      @solution.language = 'c++'
      @solution.status = 'processing'

      if @solution.save
        flash[:success] = "Solution submitted successfully for #{exercise.title}!"
        # Request to solve the solution via background processing
        ConnectionWorker.perform_async(@solution.id)
        redirect_to exercise_path(session[:exercise_id])
      else
        flash[:notice] = "Solution submit failed!"
        redirect_to new_solution_path(session[:exercise_id])
      end
    else
      flash[:notice] = "File error!"
      redirect_to new_solution_path(session[:exercise_id])
    end
  end
end
