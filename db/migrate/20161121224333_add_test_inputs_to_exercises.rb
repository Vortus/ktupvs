class AddTestInputsToExercises < ActiveRecord::Migration[5.0]
  def change
    add_column :exercises, :test_inputs, :text, array: true, default: []
  end
end
