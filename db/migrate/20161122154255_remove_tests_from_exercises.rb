class RemoveTestsFromExercises < ActiveRecord::Migration[5.0]
  def change
    remove_column :exercises, :tests, :json
  end
end
