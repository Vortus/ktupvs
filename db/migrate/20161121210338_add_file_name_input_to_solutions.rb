class AddFileNameInputToSolutions < ActiveRecord::Migration[5.0]
  def change
    add_column :solutions, :file_name_input, :string
  end
end
