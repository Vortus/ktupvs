class ExerciseGroupsController < ApplicationController
  before_action :authenticate_user!

  def index
    @exercise_groups = ExerciseGroup.all.order('updated_at DESC')
  end

  def show
    # TODO
  end

  def new
    @exercise_group = ExerciseGroup.new
  end

  def edit
    @exercise_group = ExerciseGroup.find(params[:id])
  end

  def update
    @exercise_group = ExerciseGroup.find(params[:id])
    if @exercise_group.update(exercise_group_params)
      flash[:success] = "Exercise Group was edited successfully!"
      redirect_to exercise_group_path(@exercise_group)
    else
      flash[:alert] = @exercise_group.errors.full_messages
      render 'edit'
    end
  end

  def destroy
    @exercise_group = ExerciseGroup.find(params[:id])
    if @exercise_group.destroy
      flash[:success] = "Exercise Group was removed!"
    else
      flash[:error] = @exercise_group.errors.full_messages
    end
    redirect_to exercise_groups_path
  end

  def create
    @exercise_group = ExerciseGroup.new(exercise_group_params)
    if @exercise_group.save
      flash[:success] = "Exercise Group has been created!"
      redirect_to exercise_groups_path
    else
      flash[:error] = @exercise_group.errors.full_messages
      redirect_to new_exercise_group_path
    end
  end

  private
    def exercise_group_params
      params.require(:exercise_group).permit(:title)
    end
end
