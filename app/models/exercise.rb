class Exercise < ApplicationRecord
  has_many :solutions
  has_many :exercise_tests
  accepts_nested_attributes_for :exercise_tests, reject_if: :all_blank, allow_destroy: true, allow_destroy: true
  before_save :default_values

  # Default values.
  def default_values
    self.solution_states ||= {}
  end

  # Returs exercise status by user id.
  def get_status(user_id)
    self.solution_states["#{user_id}"]
  end
end
