class CreateExerciseGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :exercise_groups do |t|
      t.string :title
      t.boolean :open

      t.timestamps
    end
  end
end
