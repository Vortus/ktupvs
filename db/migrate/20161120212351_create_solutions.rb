class CreateSolutions < ActiveRecord::Migration[5.0]
  def change
    create_table :solutions do |t|
      t.string :title
      t.string :code
      t.integer :exercise_id
      t.timestamps
    end
    add_index :solutions, :exercise_id
  end
end
