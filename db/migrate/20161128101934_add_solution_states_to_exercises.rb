class AddSolutionStatesToExercises < ActiveRecord::Migration[5.0]
  def change
    add_column :exercises, :solution_states, :json
  end
end
