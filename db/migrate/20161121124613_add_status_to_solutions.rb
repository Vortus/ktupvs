class AddStatusToSolutions < ActiveRecord::Migration[5.0]
  def change
    add_column :solutions, :status, :string
  end
end
