class StaticPagesController < ApplicationController

  def news
    @posts = Post.all.order('created_at DESC')
  end

  def exercises
    # TODO
  end

  def about
    # TODO
  end
end
