class AddFileNameToSolutions < ActiveRecord::Migration[5.0]
  def change
    add_column :solutions, :file_name, :string
  end
end
