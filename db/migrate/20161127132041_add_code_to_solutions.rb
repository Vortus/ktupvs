class AddCodeToSolutions < ActiveRecord::Migration[5.0]
  def change
    add_column :solutions, :code, :text
  end
end
