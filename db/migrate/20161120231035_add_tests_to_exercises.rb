class AddTestsToExercises < ActiveRecord::Migration[5.0]
  def change
    add_column :exercises, :tests, :json
  end
end
