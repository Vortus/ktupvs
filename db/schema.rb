# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161128101934) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "exercise_groups", force: :cascade do |t|
    t.string   "title"
    t.boolean  "open"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exercise_tests", force: :cascade do |t|
    t.text     "test_input"
    t.text     "test_output"
    t.integer  "exercise_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["exercise_id"], name: "index_exercise_tests_on_exercise_id", using: :btree
  end

  create_table "exercises", force: :cascade do |t|
    t.string   "title"
    t.string   "shorten"
    t.string   "description"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.json     "solution_states"
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.string   "shorten"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "solutions", force: :cascade do |t|
    t.string   "title"
    t.integer  "exercise_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "language"
    t.integer  "user_id"
    t.string   "status"
    t.string   "error"
    t.string   "file_name_input"
    t.string   "file_name_output"
    t.integer  "time_limit"
    t.integer  "size_limit"
    t.text     "code"
    t.index ["exercise_id"], name: "index_solutions_on_exercise_id", using: :btree
  end

  create_table "user_levels", force: :cascade do |t|
    t.string   "title"
    t.integer  "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "points"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "year_categories", force: :cascade do |t|
    t.string   "title"
    t.boolean  "open"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
