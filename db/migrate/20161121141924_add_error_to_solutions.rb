class AddErrorToSolutions < ActiveRecord::Migration[5.0]
  def change
    add_column :solutions, :error, :string
  end
end
