require 'test_helper'

class ExerciseGroupsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get exercise_groups_index_url
    assert_response :success
  end

  test "should get show" do
    get exercise_groups_show_url
    assert_response :success
  end

  test "should get new" do
    get exercise_groups_new_url
    assert_response :success
  end

  test "should get edit" do
    get exercise_groups_edit_url
    assert_response :success
  end

end
