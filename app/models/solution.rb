class Solution < ApplicationRecord
  belongs_to :exercise
  belongs_to :user
  before_save :default_values

  # Set default values.
  def default_values
    self.title ||= 'No title'
    self.error ||= ''
    self.time_limit ||= 10 # Seconds
    self.size_limit ||= 10 # MB
  end

  # Update exercise status which solution belongs to according to user id.
  def update_exercise_status
    status = self.exercise.solution_states[self.user_id]
    if status != "ok" # If solution is not solved, set status to the latest upload
      self.exercise.solution_states[self.user_id] = self.status
      self.exercise.save
      # TODO Add points to user
    end
  end
end
