class AddFileNameOutputToSolutions < ActiveRecord::Migration[5.0]
  def change
    add_column :solutions, :file_name_output, :string
  end
end
