class RemoveTestInputsFromExercises < ActiveRecord::Migration[5.0]
  def change
    remove_column :exercises, :test_inputs, :text
  end
end
