class ExercisesController < ApplicationController
  before_action :authenticate_user!

  def index
    @exercises = Exercise.all.order('updated_at DESC')
  end

  def show
    @exercise = Exercise.find(params[:id])
  end

  def new
    @exercise = Exercise.new
  end

  def edit
    @exercise = Exercise.find(params[:id])
  end

  def update
    @exercise = Exercise.find(params[:id])
    if @exercise.update(exercise_params)
      flash[:success] = "Exercise was edited successfully!"
      redirect_to exercise_path(@exercise)
    else
      flash[:alert] = @exercise.errors.full_messages
      render 'edit'
    end
  end

  def destroy
    @exercise = Exercise.find(params[:id])
    if @exercise.destroy
      flash[:success] = "Exercise was removed!"
    else
      flash[:error] = @exercise.errors.full_messages
    end
    redirect_to exercises_path
  end

  def create
    @exercise = Exercise.new(exercise_params)
    if @exercise.save
      flash[:success] = "Exercise has been created!"
      redirect_to exercise_path(@exercise)
    else
      flash[:error] = @exercise.errors.full_messages
      redirect_to new_exercise_path
    end
  end

  private
    def exercise_params
      params.require(:exercise).permit(
        :title, :shorten, :description,
        exercise_tests_attributes: [:id, :test_input, :test_output, :_destroy])
    end
end
