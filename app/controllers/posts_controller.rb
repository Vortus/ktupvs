class PostsController < ApplicationController
  before_action :authenticate_user!, :except => [:show, :index]

  def new
    @post = Post.new
  end

  def index
    @posts = Post.all.order('created_at DESC')
  end

  def show
    @post = Post.find(params[:id])
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    if @post.update(post_params)
      flash[:success] = "Post was edited successfully!"
      redirect_to post_path(@post)
    else
      flash[:alert] = @post.errors.full_messages
      render 'edit'
    end
  end

  def destroy
    @post = Post.find(params[:id])
    if @post.destroy
      flash[:success] = "Post was removed!"
    else
      flash[:error] = @post.errors.full_messages
    end
    redirect_to posts_path
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      flash[:success] = "Post has been created!"
      redirect_to post_path(@post)
    else
      flash[:error] = @post.errors.full_messages
      redirect_to new_post_path
    end
  end

  private
    def post_params
      params.require(:post).permit(:title, :shorten, :description)
    end
end
