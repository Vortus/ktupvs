class RemoveCodeFromSolutions < ActiveRecord::Migration[5.0]
  def change
    remove_column :solutions, :code, :string
  end
end
