class RemoveFileNameFromSolutions < ActiveRecord::Migration[5.0]
  def change
    remove_column :solutions, :file_name, :string
  end
end
