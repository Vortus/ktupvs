require 'sidekiq/web'
Rails.application.routes.draw do
  mount Sidekiq::Web => "/sidekiq"

  root 'static_pages#news' # Set root page as news.
  get 'solve-exercises', to: 'static_pages#exercises', as: 'static_exercises' # Solvable exercise list.
  get 'solutions/new/:id', to: 'solutions#new', as: 'new_solution' # New solution.
  get 'solutions/filter/:id', to: 'solutions#filter', as: 'filter_solution' # Filter solution

  resources :posts
  resources :solutions, except: [:new, :index]
  resources :exercises
  resources :year_categories
  resources :exercise_groups
  resources :user_levels
  devise_for :users
end
