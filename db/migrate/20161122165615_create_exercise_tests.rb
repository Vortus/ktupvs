class CreateExerciseTests < ActiveRecord::Migration[5.0]
  def change
    create_table :exercise_tests do |t|
      t.text :test_input
      t.text :test_output
      t.integer :exercise_id
      t.timestamps
    end
    add_index :exercise_tests, :exercise_id
  end
end
